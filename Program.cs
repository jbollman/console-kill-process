﻿using System;
using System.Diagnostics;
using System.Threading;

namespace list_processes
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter a Process that you want to kill, and stay stopped until this program ends? ");
            string line = Console.ReadLine(); // Get string from user
           
            Console.Write("You typed "); // Report output
            Console.Write(line.Length);

            while (true)
            {
                if (line == "exit") // Check string
                {
                    break;
                }

                try
               {

                    GetSpecificProcess(line);
               }
               catch (System.IndexOutOfRangeException)
               {
                    System.Console.WriteLine("The Proces you specified is not runnign atm. Wil continue to monitor and kill it if i see it again.");
               }
                Thread.Sleep(900);

                }
            }
        static string GPS()
        {
            Process[] processlist = Process.GetProcesses();

            foreach (Process theprocess in processlist)
            {
                var pName = theprocess.ProcessName;

                Console.WriteLine(pName);
            }
            return processlist[0].ProcessName;
        }
        static void GetSpecificProcess(string processName)
        {
            Process[] gp = Process.GetProcessesByName(processName);
            Process stringgp = gp[0];
            KillProcess(stringgp);
        }

        static void KillProcess(Process processName)
        {
            processName.Kill();
        }
    }
}